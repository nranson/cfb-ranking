#!flask/bin/python
import sys
import site
import re
from flask import Flask, jsonify, abort, make_response, url_for
from StringIO import StringIO
from flaskext.mysql import MySQL
from statistics import mean, stdev, variance

mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = 'cfb_api'
app.config['MYSQL_DATABASE_PASSWORD'] = 'cfb123'
app.config['MYSQL_DATABASE_DB'] = 'cfb_api'
app.config['MYSQL_DATABASE_HOST'] = '127.0.0.1'
mysql.init_app(app)

sos_spread = []
scores_spread = []
rank_spread = []

################################################
## Max points is the same regardless of team. ##
################################################
max_points = 1000 + (10 * int(sys.argv[1]))

##########################
## The ranking function ##
##########################
def run_ranking():
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT team_name FROM rank WHERE (wins + losses  > 5) ORDER BY points DESC")
	ranking = cursor.fetchall()
	i = 1
		
	for team in ranking:
		team = team[0]
		cursor.execute("UPDATE rank SET rank = %d WHERE team_name = '%s'" % (i, team))
		conn.commit()
		i += 1

	conn.commit()
	cursor.close()


######################################################
## Calculate recursive stregnth of schedule, weekly ##
######################################################
def strength_of_schedule(team, max_points):
	sos = 0
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT * FROM games WHERE (home_team = '%s' OR away_team = '%s') AND season = '%s'" % (team, team, sys.argv[2]))
	games = cursor.fetchall()

	for game in games:
		if game[0] == team:
			cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % game[2])
			opponent = cursor.fetchone()
			if opponent == None:
				opponent_score = max_points - (2 * int(sys.argv[1]))
			else:
				opponent_score = opponent[0]

			opponent_strength = max_points - opponent_score

		if game[2] == team:
			cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % game[0])
			opponent = cursor.fetchone()
			if opponent == None:
				opponent_score = max_points - (2 * int(sys.argv[1]))
			else:
				opponent_score = opponent[0]

			opponent_strength = max_points - opponent_score


		sos = sos + opponent_strength
	cursor.execute("UPDATE rank SET strength_of_schedule = %d WHERE team_name = '%s'" % (sos, team))
	conn.commit()

	sos_spread.append(sos)

#########################################
## Based on SoS, normalize the results ##
#########################################
def percentile(team, mean, stdev):
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT strength_of_schedule FROM rank WHERE team_name = '%s'" % team)
	points = cursor.fetchone()
	
	top = points[0] - float(mean)

	pct = top/stdev

	cursor.execute("UPDATE rank SET pct = %f WHERE team_name = '%s'" %(pct, team))
	conn.commit()

###############################
## Get scores from this week ##
###############################
def get_scores():
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT * FROM games WHERE week <= %d AND season = %d" % (int(sys.argv[1]), int(sys.argv[2])))
	scores = cursor.fetchall()

	for score in scores:
		diff = abs(score[1] - score[3])
		scores_spread.append(diff)

###############################################
## Compare rank differentials from the week. ##
###############################################
def get_rank_diff(max_points):
	conn = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT home_team, away_team FROM games WHERE week = %d AND season = %d" % (int(sys.argv[1]), int(sys.argv[2])))
	ranks = cursor.fetchall()

	for rank in ranks:
		cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % rank[0])
		home = cursor.fetchone()
		if home == None:
			home = max_points
		else:
			home = home[0]

		cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % rank[1])
		away = cursor.fetchone()
		if away == None:
			away = max_points
		else:
			away = away[0]

		spread = abs(home - away)
		rank_spread.append(spread)

################################
## Home team winner function. ##
################################
def home_team_winner(team, home_score, opponent, away_score, std, avg, opp_avg, opp_std):
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT pct, rank FROM rank WHERE team_name = '%s'" % team)
	sos = cursor.fetchone()
	rank = sos[1]
	sos = sos[0]

	cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % opponent)
	opponent = cursor.fetchone()
	if opponent == None:
		opponent = 200 + (2 * int(sys.argv[1]))
	else:
		opponent = opponent[0]

	opp_score = ((rank - opponent) - opp_avg) / opp_std
	cursor.execute("UPDATE rank SET opp_diff = %f WHERE team_name = '%s'" %(opp_score, team))
	conn.commit()

	cursor.execute("SELECT points FROM rank WHERE team_name = '%s'" % team)
	score = cursor.fetchone()
	if score == None:
		score = 0.0
	else:
		score = score[0]

	diff = float(((home_score - away_score) - avg) / std)

	#win bonus, no home bonus
	cursor.execute("UPDATE rank SET score_diff = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()
	diff = diff + sos + score + opp_score
	cursor.execute("UPDATE rank SET points = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()

	cursor.execute("SELECT wins FROM rank WHERE team_name = '%s'" % team)
	win = cursor.fetchone()
	if win[0] == None:
		win = int(0)
	else:
		win = win[0]

	win = int(win) + 1
	cursor.execute("UPDATE rank SET wins = %d WHERE team_name = '%s'" %(win, team))
	conn.commit()
	cursor.close()

##############################
## Home team loser function ##
##############################
def home_team_loser(team, home_score, opponent, away_score, std, avg, opp_avg, opp_std):
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT pct, rank FROM rank WHERE team_name = '%s'" % team)
	sos = cursor.fetchone()
	rank = sos[1]
	sos = sos[0]

	cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % opponent)
	opponent = cursor.fetchone()
	if opponent == None:
		opponent = 200 + (2 * int(sys.argv[1]))
	else:
		opponent = opponent[0]

	opp_score = ((rank - opponent) - opp_avg) / opp_std

	cursor.execute("UPDATE rank SET opp_diff = %f WHERE team_name = '%s'" %(opp_score, team))
	conn.commit()

	cursor.execute("SELECT points FROM rank WHERE team_name = '%s'" % team)
	score = cursor.fetchone()
	if score == None:
		score = 0.0
	else:
		score = score[0]

	diff = float(((home_score - away_score) - avg) / std)
	diff = diff
	cursor.execute("UPDATE rank SET score_diff = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()

	diff = diff + sos + score + opp_score - 15
	cursor.execute("UPDATE rank SET points = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()

	cursor.execute("SELECT losses FROM rank WHERE team_name = '%s'" % team)
	loss = cursor.fetchone()
	if loss[0] == None:
		loss = int(0)
	else:
		loss = loss[0]

	loss = int(loss) + 1
	cursor.execute("UPDATE rank SET losses = %d WHERE team_name = '%s'" %(loss, team))
	conn.commit()
	cursor.close()

################################
## Away team winner funciton. ##
################################
def away_team_winner(opponent, home_score, team, away_score, std, avg, opp_avg, opp_std):
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT pct, rank FROM rank WHERE team_name = '%s'" % team)
	sos = cursor.fetchone()
	rank = sos[1]
	sos = sos[0]

	cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % opponent)
	opponent = cursor.fetchone()
	if opponent == None:
		opponent = 200 + (2 * int(sys.argv[1]))
	else:
		opponent = opponent[0]

	opp_score = ((rank - opponent) - opp_avg) / opp_std

	cursor.execute("UPDATE rank SET opp_diff = %f WHERE team_name = '%s'" %(opp_score, team))
	conn.commit()

	cursor.execute("SELECT points FROM rank WHERE team_name = '%s'" % team)
	score = cursor.fetchone()
	if score == None:
		score = 0.0
	else:
		score = score[0]

	diff = float(((away_score - home_score) - avg) / std)

	#win bonus, road win bonus
	cursor.execute("UPDATE rank SET score_diff = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()

	diff = diff + sos + score + opp_score
	cursor.execute("UPDATE rank SET points = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()

	cursor.execute("SELECT wins FROM rank WHERE team_name = '%s'" % team)
	win = cursor.fetchone()
	if win[0] == None:
		win = int(0)
	else:
		win = win[0]

	win = int(win) + 1
	cursor.execute("UPDATE rank SET wins = %d WHERE team_name = '%s'" %(win, team))
	conn.commit()
	cursor.close()

###############################
## Away team loser function. ##
###############################
def away_team_loser(opponent, home_score, team, away_score, std, avg, opp_avg, opp_std):
	conn   = mysql.connect()
	cursor = conn.cursor()
	cursor.execute("SELECT pct, rank FROM rank WHERE team_name = '%s'" % team)
	sos = cursor.fetchone()
	rank = sos[1]
	sos = sos[0]

	cursor.execute("SELECT rank FROM rank WHERE team_name = '%s'" % opponent)
	opponent = cursor.fetchone()
	if opponent == None:
		opponent = 200 + (2 * int(sys.argv[1]))
	else:
		opponent = opponent[0]

	opp_score = ((rank - opponent) - opp_avg) / opp_std

	cursor.execute("UPDATE rank SET opp_diff = %f WHERE team_name = '%s'" %(opp_score, team))
	conn.commit()

	cursor.execute("SELECT points FROM rank WHERE team_name = '%s'" % team)
	score = cursor.fetchone()
	if score == None:
		score = 0.0
	else:
		score = score[0]

	diff = float(((away_score - home_score) - avg) / std)
	diff = diff
	cursor.execute("UPDATE rank SET score_diff = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()
	diff = diff + sos + score + opp_score - 10
	cursor.execute("UPDATE rank SET points = %f WHERE team_name = '%s'" %(diff, team))
	conn.commit()

	cursor.execute("SELECT losses FROM rank WHERE team_name = '%s'" % team)
	loss = cursor.fetchone()
	if loss[0] == None:
		loss = int(0)
	else:
		loss = loss[0]

	loss = int(loss) + 1
	cursor.execute("UPDATE rank SET losses = %d WHERE team_name = '%s'" %(loss, team))
	conn.commit()
	cursor.close()

##########################################################
## Get the scores from this week and all previous weeks ##
##########################################################
get_scores()
score_avg = mean(scores_spread)
score_std = stdev(scores_spread)

get_rank_diff(max_points)
rank_avg = mean(rank_spread)
rank_std = stdev(rank_spread)

conn   = mysql.connect()
cursor = conn.cursor()
cursor.execute("SELECT team_name,rank FROM rank")
teams = cursor.fetchall()

for team in teams:
	strength_of_schedule(team[0], max_points)

#######################################################
## Now that we have the strength of schedule spread  ##
##  and point diff go ahead and run some basic stats.##
#######################################################
sos_avg = mean(sos_spread)
sos_std = stdev(sos_spread)

#####################################
## This is where the magic happens ##
#####################################
cursor.execute("SELECT team_name FROM rank")
teams = cursor.fetchall()
for team in teams:
	percentile(team[0], sos_avg, sos_std)

	cursor.execute("SELECT * FROM games WHERE home_team = '%s' AND week = %d AND season = %d" % (team[0], int(sys.argv[1]), int(sys.argv[2])))
	game = cursor.fetchone()
	if game != None:
		if team[0] == game[0]:
			if team[0] == game[4]:
				home_team_winner(team[0], game[1], game[2], game[3], score_std, score_avg, 0, rank_std)
		 	if team[0] == game[5]:
		 		home_team_loser(team[0], game[1], game[2], game[3], score_std, score_avg, 0, rank_std)
		
	cursor.execute("SELECT * FROM games WHERE away_team = '%s' AND week = %d AND season = %d" % (team[0], int(sys.argv[1]), int(sys.argv[2])))
	game = cursor.fetchone()		
	if game != None:
		if team[0] == game[2]:
			if team[0] == game[4]:
		 		away_team_winner(game[0], game[1], team[0], game[3], score_std, score_avg, 0, rank_std)
		 	if team[0] == game[5]:
		 		away_team_loser(game[0], game[1], team[0], game[3], score_std, score_avg, 0, rank_std)

conn.close()
run_ranking()

print "WEEK %d" % int(sys.argv[1])